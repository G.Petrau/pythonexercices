# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def comparer(num1, num2):
    if(num1 > num2) :
        print(f'Le plus grand nombre est {num1}')
    elif(num2 > num1) :
        print(f'Le plus grand nombre est {num2}')
    else :
        print("les nombres sont identiques")




if __name__ == '__main__':
    num1 = int(input("Premier nombre : "))
    num2 = int(input("Deuxième nombre  : "))
    comparer(num1, num2)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
