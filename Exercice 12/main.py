# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def tableDeN(n):
    # Use a breakpoint in the code line below to debug your script.
    list = []
    for i in range(1, 10) :
        print(n," x ", i , " = ", n*i)

def tables() :
    for j in range(1, 10) :
        for i in range(1, 10):
            print(j, " x ", i, " = ", j * i)
        print("-----------------------------------------------------------------------------------")

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    n = int(input("Entrer un nombres : "))
    tableDeN(n)
    print("-----------------------------------------------------------------------------------")
    tables()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
