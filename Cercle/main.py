# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

def rayonCercle(n):
    pi = 3.14
    perimetre = pi * 2 * n
    aire = pow(pi, 2) * n
    print("le perimetre est : ", perimetre)
    print("l'aire est : ", aire)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    rayon = int(input("Quel est le rayon du cercle : "))
    rayonCercle(rayon)



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
