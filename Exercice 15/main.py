# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def premier(num):
    # Use a breakpoint in the code line below to debug your script.
    if num > 1:
        for i in range(2, num // 2):
            if (num % i) == 0:
                print(num, "n'est un nombre premier")
                break
            else:
                print(num, "est un nombre premier")
                break;

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    num = int(input("Entrer un nombre : "))
    premier(num)
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
