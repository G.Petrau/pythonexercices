# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def exercice21(a):
    l = list(a)
    voyelles = ["a","e","i","o","u","y"]
    count = 0
    for i in range(0, len(a)) :
        if l[i] in voyelles :
            count += 1
    print("il y a ",count,"voyelles dans le mot")

def exercice22(b) :
    l = b.split(" ")
    print("le premier mot est", l[0])
def exercice23(c):
    l = c.split(".")
    print("l'extension du fichier est", l[1])
# Press the green button in the gutter to run the script.
def exercice24(mot):
    for i in range(1, len(mot) // 2) :
        if mot[i] != mot[-i-1] :
            return False
    return True
def exercice25(mot):
    mot_inverse = mot[::-1]
    print(mot_inverse)

def exercice26(texte):
    l = texte.split(" ")
    listesMots = []
    for i in range (0, len(l)) :
        if l[i][0] == "a" :
            listesMots.append(l[i])

    print(str(listesMots))

if __name__ == '__main__':
    a = input("Entrer un mot : ")
    exercice21(a)
    b = input("Entrer une phrase : ")
    exercice22(b)
    c = input("Enter un nom de fichier : ")
    exercice23(c)
    palindrome = input("Enter un mot : ")
    if(exercice24(palindrome)) :
        print("Le mot est un palindrome")
    else :
        print("le mot n'est pas un palindrome")
    exercice25(palindrome)
    texte = input("Entrer une phrase : ")
    exercice26(texte)
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
