# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def nombresPremiers():
    # Use a breakpoint in the code line below to debug your script.
    for j in range(1, 101):
        count = 0
        for i in range(2, (j // 2 + 1)):
            if (j % i == 0):
                count = count + 1
                break

        if (count == 0 and j != 1):
            print(" %d" % j, end='  ')

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    nombresPremiers()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
