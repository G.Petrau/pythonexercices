# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def plusGrand(list):
    # Use a breakpoint in the code line below to debug your script.
    print("Le plus grand nombre de la liste est : ", max(list))



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    a = list(map(int, input("Entre trois nombres : ").rstrip().split()))
    plusGrand(a)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
