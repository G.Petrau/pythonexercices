# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import re

def exercice27(a):
    # Use a breakpoint in the code line below to debug your script.
    l = a.split(" ")
    longest = sorted(l, key = len, reverse = True)
    print("le mot le plus long est", longest[0])

def exercice28(a):
    if len(a) == 0 :
        print("Le string est vide")
    else :
        print("Le string n'est pas vide")
def exercice29(a):
    list = set(a)

    print(list)
def exercice30(l1, l2) :
    l1_set = set(l1)
    intersection = l1_set.intersection(l2)
    intersection_list = list(intersection)
    print(intersection_list)

def exercice31(liste):
    liste_pairs = []
    liste_impairs = []
    for i in liste :
        if i % 2 == 0 :
            liste_pairs.append(liste[i])
        else :
            liste_impairs.append(liste[i])
    print("les nombres pairs sont ", str(liste_pairs))
    print("les nombres impairs sont ", str(liste_pairs))
# Press the green button in the gutter to run the script.
def exercice33(liste):
    print(str(liste[::2]))

def exercice34(listeNotes) :
    print([(i) for i in listeNotes if i >= 10])

def exercice35(a):
    l = a.split(" ")
    for i in range(0, len(l)) :
        b = l.count(l[i])
        print(l[i], " apparait ", b ," fois dans la phrase")

def exercice36(phrase):
    print(re.sub(" ", '', phrase))

def exercice37(l1, l2) :
    l1 = l1.split()
    l2 = l2.split()
    l1_set = set(l1)
    intersection = l1_set.intersection(l2)
    intersection_list = list(intersection)
    print("les valeurs communes sont : ", intersection_list)

def exercice38(a):
    l = a.split(" ")
    print(" ".join([l[-1]] + l[1:-1] + [l[0]]))

def exercice39(a):
    l = a.split(" ")
    print("il y a ",len(l), "mots dans la chaine")


if __name__ == '__main__':

    a = input("Enter une phrase : ")
    exercice27(a)
    exercice28(a)
    liste = input("Entrer une liste : ").split(" ")
    exercice29(liste)
    listeNombres = list(map(int, input("Enter une liste de nombres : ").rstrip().split()))
    exercice31(listeNombres)
    exercice33(listeNombres)
    exercice34(listeNombres)
    exercice36()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
