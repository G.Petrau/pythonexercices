from math import *

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

def perfectSquare(n):

    if int(math.sqrt(n) * 0.5) ** 2 == n :
        print("Le nombre est un carré parfait")

    else :
        print("Le nombre n'est pas un carré parfait")



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    nombre = int(input("Entrer un nombre : "))
    perfectSquare(nombre)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
