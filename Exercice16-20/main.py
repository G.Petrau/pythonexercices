# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import string


def exercice16(a):
    l = list(a)
    print(' '.join(l))

def exercice17(a):
    l = list(a)
    for i in range(0, len(l)) :
        b = l.count(l[i])
        print(l[i], " apparait ", b ," fois dans le mot")

def exercice18(a):
    l = list(a)
    if 'a' in l :
        b = l.index('a') + 1
        print("a est a la",b,"ième position dans le mot")
    else :
        print("a n'est pas dans le mot")

def exercice19():

    l = ["laptop", "iphone", "tablet"]
    for i in range(0, len(l)) :
        print(l[i], len(l[i]))

def exercice20(a):
    l = list(a)
    temp = l[0], l[-1]
    l[0] = temp[1]
    l[-1] = temp[0]
    print(''.join(l))

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    a = input("Entrer un mot : ")
    exercice16(a)
    exercice17(a)
    print("------------------------------------------------------")
    exercice18(a)
    exercice19()
    exercice20(a)
    # See PyCharm help at https://www.jetbrains.com/help/pycharm/
