# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from math import log10

def exercice41(listeNombres, n):
    # Use a breakpoint in the code line below to debug your script.
    nombresDivisibles = []
    for i in listeNombres:
        if i % n == 0 :
            nombresDivisibles.append(i)

    print(nombresDivisibles)

def exercice42(liste, n):
    l = liste.split(" ")
    count = 0
    for i in l:
        if n in i:
           count += 1
    print(n, "est présent", count, "fois dans la liste")

def exercice43(c):
    l = list(c)
    print("*".join(l))

def exercice44(liste):
    liste = liste.split(" ")
    upperCase = []
    for i  in liste:
        upperCase.append(i.upper())
    print(upperCase)

def exercice45(s):
    r = {'countUpper': 0, 'countLower': 0}

    for c in s:
        if c.islower():
            r['countLower'] += 1

        if c.isupper():
            r['countUpper'] += 1

    return r

def exercice46(n, base = 10):
    if n == 0:
        yield 0
    while n:
        n, d = divmod(n, base)
        yield d




def exercice47(l1, l2) :
    l1 = l1.split()
    l2 = l2.split()
    l1_set = set(l1)
    intersection = l1_set.intersection(l2)
    intersection_list = list(intersection)
    print("les valeurs communes sont : ", intersection_list)



if __name__ == '__main__':
    a = input("Entrer un mot : ")
    exercice43(a)
    exercice44(a)
    print(list(reversed(tuple(exercice46(int(input("n = ")))))))

