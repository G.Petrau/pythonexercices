# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def division(a, b):
    # Use a breakpoint in the code line below to debug your script.
    quotient = a//b
    reste = a%b
    print("le quotient est : ", quotient)
    print("le reste est : ", reste)
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    num1 = int(input("a = "))
    num2 = int(input("b = "))
    division(num1, num2)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
