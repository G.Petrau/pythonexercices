
# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_name(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Bienvenue, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    name = input("Quel est ton nom ? ")
    print_name(name);

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
