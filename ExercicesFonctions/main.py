# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

def chiffreBonheur(nb):
    s = 0
    if int(nb) > 10 :
        for i in nb:
            s += int(i) ** 2
        nb = s
        chiffreBonheur(str(nb))
    elif int(nb) == 1 :
        print("Le chiffre " + nbChoix + " est un chiffre porte bonheur")
    else :
        print("Le chiffre " + nbChoix + " n'est pas porte bonheur")

def generateur(nb) :
    str=""
    for i in range(ord('a'), ord('z') + 1):
        for j in range(0, nb) :
            str += "%s, " % chr(i)
    print(str[:-2])

def powerset(listenb) :
    if len(listenb) <= 1:
        yield listenb
        yield []
    else:
        for item in powerset(listenb[1:]):
            yield [listenb[0]]+item
            yield item


def decorateur(function):
    def compter():
        compter.calls += 1
        print(compter.calls)
        return function()
    compter.calls = 0
    return compter
@decorateur
def decorer():
    print("bonjour")

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #nbChoix = input("Entrer un nombre : ")
    #chiffreBonheur(nbChoix)
    nbFois = int(input("Cb de fois voulez-vous répeter : "))
    generateur(nbFois)
    print([x for x in powerset([1, 2])])
    decorer()
    decorer()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
